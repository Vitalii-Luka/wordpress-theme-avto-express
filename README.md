# WordPress Theme Template
Demo WordPress  AVTO EXPRESS theme from scratch

## Local infrastructure ##

Local development infrastructure consists of:

```
- PHP 8.1
- Nginx
- MySQL 8.0
- phpMyAdmin
- Mailhog
```

## Getting started
Before you get started with this setup I assume that you have:
1. Installed [Lando](https://github.com/lando/lando) and gotten familiar with its basics
1. Got familiar with Lando's [WordPress recipe](https://docs.lando.dev/config/wordpress.html)
1. Read about the various services, tooling, events and routing Lando offers.

## Usage
These steps assume that Lando is already [installed](https://docs.lando.dev/basics/installation.html).

1. Download a zipped copy of this repository to your project root.
1. Run the command `lando start` from the project root.
1. Download WordPress with WP CLI by `lando wp core download --path=web`
1. Update your hosts file so that the app's domain points to 127.0.0.1 (`127.0.0.1 avto-express.loc` | `127.0.0.1 mail.avto-express.loc` | `127.0.0.1 phpmyadmin.avto-express.loc`).

## Info
Lando will automatically set up a database with a user and password and also set an environment variables called `lando info` that contains useful information about how your application can access other Lando services.
``` 
database: avto.express
username: wordpress
password: wordpress
host: database
# for mysql
port: 3306
```
Go to `phpmyadmin.avto-express.loc` to visit PHPMyAdmin and `mail.avto-express.loc` to visit MailHog.

## Documentation
Refer to Lando's extensive [documentation](https://docs.lando.dev/)