<!DOCTYPE HTML>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
  <?php wp_head(); ?>
</head>

<body>

<header class="header">
    <div class="container">
        <div class="header__top">
            <a class="logo" href="#">
                <img class="logo__img"
                     src="<?php bloginfo('template_url'); ?>/assets/images/logo.svg"
                     alt="logo">
            </a>
            <a class="phone" href="tel:+380505556677">+38 (050) 555 66 77</a>
        </div>
        <div class="header__content">
            <h1 data-wow-delay=".5s"
                class="header__title wow animate__fadeInLeft">
                авто із США і Кореї "під ключ"
            </h1>
            <h2 data-wow-delay="1s"
                class="header__subtitle wow animate__fadeInLeft">
                Авто із Кореї на замовлення - економія до 50%
            </h2>
            <p data-wow-delay="1.5s"
               class="header__text wow animate__fadeInLeft">
                Підбір, покупка, доставка, розмитнення, ремонт, постановку на
                український облік
            </p>
            <a class="button" href="#">КОНСУЛЬТАЦІЯ ЕКСПЕРТА</a>
            <div class="social header__social">
                <a class="social__link" href="#">
                    <svg class="test" width="26" height="26">
                        <use xlink:href="<?php bloginfo('template_url'); ?>/assets/images/icon/sprite.svg#instagram"></use>
                    </svg>
                </a>
                <a class="social__link" href="#">
                    <svg width="25" height="19">
                        <use xlink:href="<?php bloginfo('template_url'); ?>/assets/images/icon/sprite.svg#telegram"></use>
                    </svg>
                </a>
                <a class="social__link" href="#">
                    <svg width="26" height="26">
                        <use xlink:href="<?php bloginfo('template_url'); ?>/assets/images/icon/sprite.svg#whatsapp"></use>
                    </svg>
                </a>
                <a class="social__link" href="#">
                    <svg width="14" height="25">
                        <use xlink:href="<?php bloginfo('template_url'); ?>/assets/images/icon/sprite.svg#facebook"></use>
                    </svg>
                </a>
            </div>
            <img data-wow-delay="2s"
                 class="header__images wow animate__fadeInUpBig"
                 src="<?php bloginfo('template_url'); ?>/assets/images/ford-mustang.png"
                 alt="ford mustang">
        </div>
    </div>
</header>
